import React from 'react';

const MyComponent = ({ text }) => {
    return (
        <div>
            <h2>{text}</h2>
        </div>
    );
};

export default MyComponent;
