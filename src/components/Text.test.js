import renderer from 'react-test-renderer';
import Text from './Text';

it('renders component with text', () => {
    const component = renderer.create(<Text text="Hello, world!" />);
    let tree = component.toJSON();

    expect(tree).toMatchSnapshot();
});
