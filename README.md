# Documentation de Déploiement avec GitLab CI/CD et Docker
Ce dépôt contient les instructions pour déployer une application en utilisant GitLab CI/CD et Docker.

## Étapes du Processus de Déploiement

### Clonage du Dépôt :

```bash
git clone https://gitlab.com/macsimtv/evaluation-final-cicd
cd evaluation-final-cicd
```

Chaque fois qu'un push est effectué sur la branche configurée dans .gitlab-ci.yml, GitLab CI/CD démarre le pipeline, construit l'image Docker et déploie l'application.